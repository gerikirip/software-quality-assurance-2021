# Software Quality Assurance 2021

## version control with git

Basic forking workflow:

* `Fork` project
* `clone` project
* make changes
* `git add *`
* `git commit -m <commit message>`
* `git push` ...
* make `merge request`
